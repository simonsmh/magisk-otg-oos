#### OxygenOS OTG Enabler

OxygenOS OTG Enabler was designed to enable OTG on start & make the choice persist.
**Fxxk** Oneplus for its **ALWAYS SETTLE** statement.

#### NOTICE

* You should use latest Magisk Manager to install this module. If you meet any problem under installation from Magisk Manager, please try to install it from recovery.
* Recent fixes:  
init

#### Credit & Support

* Copyright (C) 2017 simonsmh <simonsmh@gmail.com>
* Any issue or pull request is welcomed.
* Star this module at [GitHub](https://github.com/Magisk-Modules-Repo/magisk-otg-oos).